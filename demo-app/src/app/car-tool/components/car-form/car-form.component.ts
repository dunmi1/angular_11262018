import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Car } from '../../models/car';

@Component({
  selector: 'car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {

  @Input()
  buttonText = 'Submit Form';

  @Output()
  submitForm = new EventEmitter<Car>();

  carForm: FormGroup;

  carMakes = [ 'Ford', 'Chevrolet', 'Tesla' ];

  constructor(private fb: FormBuilder) {
    this.carForm = this.fb.group({
      make: [ '', Validators.required ],
      model: [ '', { validators: [ Validators.required, Validators.minLength(3) ] } ],
      year: [ 1900, { validators: [ Validators.required, Validators.min(1895) ] } ],
      color: '#000000',
      price: [ 0, { validators: [
        Validators.required,
        Validators.min(0),
        Validators.max(100000),
      ] } ],
    });
  }

  ngOnInit() {
  }

  doSubmitForm() {
    this.submitForm.emit({ ...this.carForm.value });
    this.carForm.reset({
      make: '',
      model: '',
      year: 1900,
      color: '#000000',
      price: 0,
    });
  }

}

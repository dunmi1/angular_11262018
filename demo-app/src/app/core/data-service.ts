import { Item } from './item';

export abstract class DataService<T extends Item> {

  constructor(private items: T[] = []) { }

  all() {
    return this.items.concat();
  }

  append(item: T) {
    this.items = [ ...this.items, Object.assign({}, item, {
      id: Math.max(...this.items.map(i => i.id)) + 1,
    }) ];
    return this;
  }

  delete(itemId: number) {
    this.items = this.items.filter(i => i.id !== itemId);
    return this;
  }

  replace(item: T) {
    const itemIndex = this.items.findIndex(i => i.id === item.id);
    this.items = [ ...this.items.slice(0, itemIndex), item, ...this.items.slice(itemIndex + 1) ];
    return this;
  }
}

import { Injectable } from '@angular/core';

import { Color } from '../models/color';

@Injectable({
  providedIn: 'root',
})
export class ColorsService {

  private colors: Color[] = [
    { id: 1, name: 'blue', hexCode: '0000FF' },
    { id: 2, name: 'black', hexCode: '000000' },
    { id: 3, name: 'gray', hexCode: 'CCCCCC' },
    { id: 4, name: 'purple', hexCode: 'FF00FF' },
  ];

  constructor() { }

  all() {
    return this.colors.concat();
  }
}

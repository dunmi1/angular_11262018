import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyToolRoutingModule } from './company-tool.routing';

import { HomePageComponent } from './components/home-page/home-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { ContactPageComponent } from './components/contact-page/contact-page.component';

@NgModule({
  declarations: [HomePageComponent, AboutPageComponent, ContactPageComponent],
  imports: [
    CommonModule, CompanyToolRoutingModule
  ]
})
export class CompanyToolModule { }

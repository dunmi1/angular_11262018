import { FormGroup } from '@angular/forms';

export const validatorSpecifyEmailOrPhone = (fg: FormGroup) => {

  if ((fg.controls.email.value == null || String(fg.controls.email.value).length === 0)
    && (fg.controls.phone.value == null || String(fg.controls.phone.value).length === 0)) {
      return {
        emailOrPhone: 'Please specify an email address or phone number.',
      };
  }

  return null;
};